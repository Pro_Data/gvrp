# A green vehicle routing problem with time windows considering the heterogeneous fleet of vehicles: Two meta-heuristic algorithms #

### What is this repository for? ###

This is a repository for the input data of all random instances generated in this paper. The interested readers can use the following data for any future comparison. 

- Instance 1:

https://bitbucket.org/Pro_Data/gvrp/downloads/1_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/1_Part2.xlsx

- Instance 2:

https://bitbucket.org/Pro_Data/gvrp/downloads/2_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/2_Part2.xlsx

- Instance 3:

https://bitbucket.org/Pro_Data/gvrp/downloads/3_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/3_Part2.xlsx

- Instance 4:

https://bitbucket.org/Pro_Data/gvrp/downloads/4_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/4_Part2.xlsx

- Instance 5:

https://bitbucket.org/Pro_Data/gvrp/downloads/5_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/5_Part2.xlsx

- Instance 6:

https://bitbucket.org/Pro_Data/gvrp/downloads/6_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/6_Part2.xlsx

- Instance 7:

https://bitbucket.org/Pro_Data/gvrp/downloads/7_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/7_Part2.xlsx

- Instance 8:

https://bitbucket.org/Pro_Data/gvrp/downloads/8_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/8_Part2.xlsx

- Instance 9:

https://bitbucket.org/Pro_Data/gvrp/downloads/9_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/9_Part2.xlsx

- Instance 10:

https://bitbucket.org/Pro_Data/gvrp/downloads/10_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/10_Part2.xlsx

- Instance 11:

https://bitbucket.org/Pro_Data/gvrp/downloads/11_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/11_Part2.xlsx

- Instance 12:

https://bitbucket.org/Pro_Data/gvrp/downloads/12_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/12_Part2.xlsx

- Instance 13:

https://bitbucket.org/Pro_Data/gvrp/downloads/13_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/13_Part2.xlsx

- Instance 14:

https://bitbucket.org/Pro_Data/gvrp/downloads/14_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/14_Part2.xlsx

- Instance 15:

https://bitbucket.org/Pro_Data/gvrp/downloads/15_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/15_Part2.xlsx

- Instance 16:

https://bitbucket.org/Pro_Data/gvrp/downloads/16_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/16_Part2.xlsx

- Instance 17:

https://bitbucket.org/Pro_Data/gvrp/downloads/17_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/17_Part2.xlsx

- Instance 18:

https://bitbucket.org/Pro_Data/gvrp/downloads/18_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/18_Part2.xlsx

- Instance 19:

https://bitbucket.org/Pro_Data/gvrp/downloads/19_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/19_Part2.xlsx

- Instance 20:

https://bitbucket.org/Pro_Data/gvrp/downloads/20_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/20_Part2.xlsx

- Instance 21:

https://bitbucket.org/Pro_Data/gvrp/downloads/21_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/21_Part2.xlsx

- Instance 22:

https://bitbucket.org/Pro_Data/gvrp/downloads/22_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/22_Part2.xlsx

- Instance 23:

https://bitbucket.org/Pro_Data/gvrp/downloads/23_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/23_Part2.xlsx

- Instance 24:

https://bitbucket.org/Pro_Data/gvrp/downloads/24_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/24_Part2.xlsx

- Instance 25:

https://bitbucket.org/Pro_Data/gvrp/downloads/25_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/25_Part2.xlsx

- Instance 26:

https://bitbucket.org/Pro_Data/gvrp/downloads/26_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/26_Part2.xlsx

- Instance 27:

https://bitbucket.org/Pro_Data/gvrp/downloads/27_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/27_Part2.xlsx

- Instance 28:

https://bitbucket.org/Pro_Data/gvrp/downloads/28_Part1.txt
https://bitbucket.org/Pro_Data/gvrp/downloads/28_Part2.xlsx